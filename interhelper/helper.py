#-*- coding:utf-8 -*-
import os,sys,django

django_pro_home="/home/ubuntu/work1/interhelper"
sys.path.append(django_pro_home)
os.environ["DJANGO_SETTINGS_MODULE"]="interhelper.settings"
django.setup()

from trans.models import RuDic
from konlpy.tag import Twitter
from konlpy.utils import pprint

def helper(text):
    t= Twitter()
    print("<td>"+text+"</td>")
    words = t.pos(text)
    for k in words:
         if k[1] == "Noun" or k[1] == "Adjective" or k[1] == "Verb":
             for e in RuDic.objects.filter(ko_text=k[0]):
                 print("<tr><td>"+e.ko_text+"</td><td>"+e.ko_desc+"</td></tr>")

text = sys.argv[1].decode('utf-8')
helper(text)