# -*- coding: utf-8 -*-
__author__ = 'parkindani'

csv_filepathname="/home/ubuntu/work1/interhelper/korustrim3.csv"
your_djangoproject_home="/home/ubuntu/work1/interhelper"

import sys,os

sys.path.append(your_djangoproject_home)

os.environ['DJANGO_SETTINGS_MODULE'] = 'interhelper.settings'

from trans.models import RuDic

import csv

dataReader = csv.reader(open(csv_filepathname), delimiter=',', quotechar='"')

for row in dataReader:
    if row[0] != 'Ruword': # Ignore the header row, import everything else
        r = RuDic()
        r.ko_text = row[0]
        r.ko_desc = row[1]
        r.save()