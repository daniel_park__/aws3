from __future__ import unicode_literals

from django.db import models

class RuDic(models.Model):
	ko_text = models.CharField(max_length=500)
	ko_desc = models.CharField(max_length=2000)
	def __unicode__(self):
		return self.ko_text +" : "+ self.ko_desc