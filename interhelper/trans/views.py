#-*- coding:utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
from konlpy.tag import Mecab
from django.views.decorators.csrf import csrf_exempt
from django.template.context import RequestContext
import re


from models import RuDic

def index(request):
	return HttpResponse("Hello, World. This is trans home")

def helper(request, ko_id):
	return HttpResponse("You're looking at %s" % ko_id)
	
def wordfinder(request):
	templist = ['this','is','sparta']
	context = {'templist' : templist}
	return render(request,'wordfinder/wordfinder.html', context)

def firstView(request):
    return render(request, 'RuDic/dic.html')

@csrf_exempt
def sentenceParsing(request):
    text = request.POST['text']
    t = Mecab()
    words = t.pos(text)
    output = "<hr><li><b>"+text+"</b></li><br>"
    for ktuple in words:
        k = list(ktuple)
        if k[1] == "NNG" or k[1] == "NNP" or k[1] == "NR" or k[1] == "VV" or k[1] == "VA":
        	if k[1] == "VV":
        	   k[0] = k[0]+u"다"
        	else:
        		pass
                resultArr = RuDic.objects.filter(ko_text=k[0]).all()
                for e in resultArr:
                    outText = "<li><b>" +e.ko_text +"</b> : "+ e.ko_desc + "</li>"
                    output+= re.sub(u'([\u0400-\u0500]+)','<span>'+r'\1'+'</span>',outText)
    context = {'output' : output}
    return render(request, 'RuDic/response.html', context)
