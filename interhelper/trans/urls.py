from django.conf.urls import url
from . import views

urlpatterns = [
	#url(r'^$', views.index, name='index'),
	url(r'^(?P<ko_id>[0-9]+)/$', views.helper, name='helper'),
	url(r'^wordfinder$', views.wordfinder, name = 'wordfinder'),
	url(r'^$', views.firstView),
    url(r'^post/$', views.sentenceParsing),
]