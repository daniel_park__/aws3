
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
	url(r'^trans/', include('trans.urls')),
    url(r'^admin/', admin.site.urls),
]
