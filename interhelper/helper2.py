#-*- coding:utf-8 -*-
import os,sys,django

django_pro_home="/home/ubuntu/work1/interhelper"
sys.path.append(django_pro_home)
os.environ["DJANGO_SETTINGS_MODULE"]="interhelper.settings"
django.setup()

from trans.models import RuDic
from konlpy.tag import Twitter, Mecab
from konlpy.utils import pprint

def helper(text):
    t= Mecab()
    print("<td>"+text+"</td>")
    words = t.pos(text)
    for k in words:
         if k[1] == "NNG" or k[1] == "NNP" or k[1] == "NR" or k[1] == "VV" or k[1] == "VA":
             for e in RuDic.objects.filter(ko_text=k[0]):
                 print("<tr><td>"+e.ko_text+"</td><td>"+e.ko_desc+"</td></tr>")

# class helper:
# 	def helper(text):
# 		t= Mecab()
# 		print("<td>"+text+"</td>")
# 		words = t.pos(text)
# 		for k in words:
# 			if k[1] == "NNG" or k[1] == "NNP" or k[1] == "NR" or k[1] == "VV" or k[1] == "VA":
# 				for e in RuDic.objects.filter(ko_text=k[0]):
# 					print("<tr><td>"+e.ko_text+"</td><td>"+e.ko_desc+"</td></tr>")
				 
text = sys.argv[1].decode('utf-8')
helper(text)